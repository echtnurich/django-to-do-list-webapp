# Django To Do List Webapp
# Table of Contents
[[_TOC_]] 
# Quick Start

1. Download Python at https://www.python.org/downloads/
2. Install django and django-crispy-forms
   -  `pip install django, django-crispy-forms`
3. Download the project folder under /src/
4. Run  `python manage.py makemigrations` and  
        `python manage.py migrate`
5. Run `python manage.py runserver`
6. Access the site under https://127.0.0.1/registration and create an user
7. Login in under https://127.0.0.1/Login
8. Start creating to do lists

# Introduction

In this Tutorial I'd like to go through a project with the Web framework Django, which is written in Python.

My first Programming Language I have learned was python.

I have been learning and using python for over a year now and since python is one of the easier Languages to learn, it's straightforward to understand even without providing any experience in programming because it's very near to the English language.

Over the course of the year, I've been working on multiple project, from minor automation and file scripting to Websites and API interfaces.
Even though Python is not the most efficient programming Language, it is still very mighty and well suited for more large-scale projects, which is, furthermore why it is one of the most popular programming languages.

One example of a bigger Project is developing a full-stack website.
One of the most popular Web frameworks for Python is Django, which I'll introduce to you in this tutorial.

In this tutorial I'll accompany you on how to develop a To-do list in Django, which will be accessible through your web browser.

What you will need:

- Basic understanding of Python 
- Basic HTML and CSS Knowldge
- Recommended but not necessary Javascript

I'll try to make this tutorial as accessible as possible, so people with no experience can follow and develop their own to-do list.

## What is Django

First of all what is Django?
Django is a Web framework to dynamaly generate websites.

But what is a framework?
A framework is a toolkit of various predefined modules within a programming language.

In this regard I'd like to go further into the functionality of Django.
As a starting point, I used the official Documentation of Django which is found here: https://docs.djangoproject.com/en/3.1/
The above tutorial is a good walk through on the functionalities of Django, where you can make your first own web application, in that case it is a poll app.


# How to create your first Django Webapplication

## Installation

First of all you have to install Python.
You can download the latest version of Python at https://www.python.org/downloads/

After you have installed python, you can make sure it's installed correctly by typing in your command line:

```bash
C:\Windows\system32>python
Python 3.8.3 (tags/v3.8.3:6f8c832, May 13 2020, 22:20:19) [MSC v.1925 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

If your Command line looks like the above, it is installed correctly.

After that make sure pip works correctly.
Pip is used to install packages and modules that can be used by Python.

```
C:\Windows\system32>pip

Usage:
  pip <command> [options]

Commands:
  install                     Install packages.
  download                    Download packages.
  uninstall                   Uninstall packages.
  freeze                      Output installed packages in requirements format.
  list                        List installed packages.
  show                        Show information about installed packages.
  check                       Verify installed packages have compatible dependencies.
  config                      Manage local and global configuration.
  search                      Search PyPI for packages.
  cache                       Inspect and manage pip's wheel cache.
  wheel                       Build wheels from your requirements.
  hash                        Compute hashes of package archives.
  completion                  A helper command used for command completion.
  debug                       Show information useful for debugging.
  help                        Show help for commands.

General Options:
  -h, --help                  Show help.
  --isolated                  Run pip in an isolated mode, ignoring environment variables and user configuration.
  -v, --verbose               Give more output. Option is additive, and can be used up to 3 times.
  -V, --version               Show version and exit.
  -q, --quiet                 Give less output. Option is additive, and can be used up to 3 times (corresponding to
                              WARNING, ERROR, and CRITICAL logging levels).
  --log <path>                Path to a verbose appending log.
  --no-input                  Disable prompting for input.
  --proxy <proxy>             Specify a proxy in the form [user:passwd@]proxy.server:port.
  --retries <retries>         Maximum number of retries each connection should attempt (default 5 times).
  --timeout <sec>             Set the socket timeout (default 15 seconds).
  --exists-action <action>    Default action when a path already exists: (s)witch, (i)gnore, (w)ipe, (b)ackup,
                              (a)bort.
  --trusted-host <hostname>   Mark this host or host:port pair as trusted, even though it does not have valid or any
                              HTTPS.
  --cert <path>               Path to alternate CA bundle.
  --client-cert <path>        Path to SSL client certificate, a single file containing the private key and the
                              certificate in PEM format.
  --cache-dir <dir>           Store the cache data in <dir>.
  --no-cache-dir              Disable the cache.
  --disable-pip-version-check
                              Don't periodically check PyPI to determine whether a new version of pip is available for
                              download. Implied with --no-index.
  --no-color                  Suppress colored output
  --no-python-version-warning
                              Silence deprecation warnings for upcoming unsupported Pythons.
  --use-feature <feature>     Enable new functionality, that may be backward incompatible.
  --use-deprecated <feature>  Enable deprecated functionality, that will be removed in the future.
```

if it looks like the above, it is installed correctly.



Now you can start installing Django

```
python -m pip install --upgrade pip
pip install django
```

## Creating a Project

After installing Django we can start making our first project.

```
django-admin startproject mysite
```

This Command will create a Project folder with the name mysite, if you want you can rename "mysite" with something your liking.

Now we can go into that folder. We should see another folder, named after our Project and a "manage.py".
To Run a web server type the following command while you're in the Project folder.

```
python manage.py runserver
```

If everything is working you will see:

```
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

December 29, 2020 - 14:32:11
Django version 3.0.7, using settings 'mysite.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.
```

Without more ado you can copy the IP address "http://127.0.0.1:8000/" and paste it into your browser.
You should see something like this:

![Django_Start](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Django_Start.png)

## Creating a new App

Now we can start making our own first application.

```
python manage.py startapp todolist
```

Now you should have a new Directory with a set of files.
Also, we can start modifying the files in the Directories.

For easier file editing, I'm using Visual Studio Code, a free File editor with some convenient tools and open the whole Project Folder with VS Code.
So it will look like this.

![Django_Start](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Django_Directory.png)


First off we have to add our App in the INSTALLED_APPS in mysite/settings.py

```python
INSTALLED_APPS=[
'...',
'todolist'
]
```

This will make referencing templates/files and classes much easier, so you don't have to call the full path of a function.



### URL Referencing

Next up we will create a new file in our app directory which will be todo/urls.py 

```python
from django.urls import path
from . import views

urlpatterns = [
    path('', views.IndexView, name='index'),
    path('<int:id>/',views.ToDoView, name='ToDoList')
]
```

and add two lines in our urls.py file under the project directory, in our case mysite/urls.py

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('lists/',include('todolist.urls'))
]
```

These two files will navigate our application to get to the right destination.
The mysite/urls.py will now look at the given URL for example "https://example.com/lists" is now referenced to the "todolist.urls" which is our "todo/urls.py" file.
The include function before the reference to "todolist.urls" is now telling Django that there is another urls file.

Therefore, if we type /lists into the URL of the Browser it will lookup and compare it with the given paths in the "mysite/urls.py" file.
If it finds the URL Pattern, it will forward us to the" todo/urls.py" file, which will also lookup the second Part of the URL after the "/".
In our case, if we type "127.0.0.1:8000/lists/" it will forward us to the "views.IndexView" function.
We have also defined another Path which forwards us to the "views.ToDoView" function, if we request "127.0.0.1/lists/{Interger}".
Integer is in this case a variable Number, which I will further explain later.

As we don't have defined our functions, referenced in the "todo/urls.py" file, we have to define them to display a website.

If we would start our server now, it should cause an error because it couldn't find the views.IndexView and views.TodoView.
In that case we create two placeholder Functions, so we can fulfill the following step.

```
def IndexView(request):
	pass
def ToDoView(request):
	pass
```



### Creating a Database model

But before we further edit our "todolist/views.py", which will render our website, we continue start making our Database Model.
Fortunately, Django has already an integrated sqlite database.
We only have to create and edit our "todolist/models.py" file:

```python
from django.db import models

class ToDoList(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class items(models.Model):
    todolist = models.ForeignKey(ToDoList, on_delete=models.CASCADE)
    text = models.CharField(max_length=300)
    complete = models.BooleanField()

    def __str__(self):
        return self.text
```

This will create two tables in our database.
A "ToDoList" Table, which will be our piece of paper with a name at the top.
And a "items" Table, which will hold our To-Dos, which will be written on the piece of paper where it is referenced to.

To write the new models to our Database, will have to migrate it.

```
python manage.py makemigrations
python manage.py migrate
```

These commands will transfer our model to the database.
If we look into the todolist/migrations/0001_initial.py file, we can see what changes were made. We also see that an Auto incremented id Field was added by default.

Now let's also add our new models to the admin Page of Django.
todolist/admin.py

```python
from django.contrib import admin
from . import models

admin.site.register(models.ToDoList)
admin.site.register(models.items)
```

This will add a predefined Editor into our webapplication, which will be accessible at "http://127.0.0.1/admin"
To access this site, we have to create an admin user

```bash
python manage.py createsuperuser
```

Simply follow the instructions in the commandline and execute the runserver command.

On the admin dashboard, we can see that a "TODOLIST" section is available, because we added it in our models.py.
Now we can add new to do lists, by clicking on the "add" button, in the "To do list" row and also add a new item and refrence it to the to do list, which we created beforehand.

![DjangoAdmin_ModelsList](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/DjangoAdmin_%20ModelsList.png)
![DjangoAdmin_TODoList](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/DjangoAdmin_TODoList.png)
![DjangoAdmin_AddItemToList](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/DjangoAdmin_AddItemToList.png)
![DjangoAdmin_AddItem](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/DjangoAdmin_AddItem.png)


### Creating HTML Templates and rendering them


A very convinent way, to keep it simple and unclusterd, is to create a base.html file. Also we follow the DRY (Don't Reapeat Yourself) principle by defining our Navigation bar once, so we don't have to repeat it everytime we create a new page.

First of all we have to create a few new directories in "todolist/templates/todolist/html" and create a base.html file.

Our first base.html file will look like that. It's very basic and only has a link to "/lists" and a Div container.
```html
<!DOCTYPE html>
<html>
    <head>
        <title>
            {% block title %}
            {% endblock %}
        </title>
    </head>
    <body>
        <div>
            <ul>
                <li><a href=/lists>home</a></li>
            </ul>
            
        </div>
        <div id="content" name="content">
            {% block content %}
            {% endblock %}
        </div>
    </body>
</html>
```

Now we can create multiple html files and reference it to the base.html file.
As an example we create a index.html file, to show the basic capabilities of a base.html file

```html
{% extends 'todolist/html/base.html' %}

{% block title %}
Title Text
{% endblock %}

{% block content %}
<h1>Hello World!</h1>
{% endblock %}
```

"{% extends 'todolist/html/base.html' %}" indecates, that we will use the contents of "base.html"
"{% block title %}" and "{% block content %}" is a refrence to the base.html file and adds the content into the corresponding block in the base.html.

But to  render the Template, we will have to edit our placeholder function in our views.py

```python
from django.shortcuts import render,redirect

def IndexView(request):
    return render(request,'todolist/html/index.html',{})
```



This function will run when the URL, defined in our urls.py, is requested. 
The django built-in render Method, takes 3 argument, the request itself, the Template, and a dictionary.
The Function will now lookup the HTML Template look for curly bracket and translate them and send a response to the requested user.

In our case we see, that we have a h1 Hello World message and the defined Title Text.

![Django_HelloWorld](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Django_HelloWorld.png)

Since everything is now set up and ready, we can now start working on the actual To Do List View.

Again we edit the views.py

```python
from .models import ToDoList,items

def ToDoView(request,id):
    list = ToDoList.objects.get(id=id)
    items = list.items_set.all()
    return render(request,'todolist/html/todolist.html',{"list":list,"items":items})
```

The directory, will allow us to call variable in the template. The string we define as the key, will be available and accessible in template.

```html
{% extends 'todolist/html/base.html' %}

{% block title %}
    {{list}}
{% endblock %}

{% block content %}
    <h1>{{ list }}</h1>
    <p>To Do:</p>
        {% for item in items %}
            {% if item.complete == True %}
        <input type="checkbox" name="c{{item.id}}" value=clicked checked> <input type="text" value="{{item.text}}" name=n{{item.id}}> <br>
        {% else %}
        <input type="checkbox" name="c{{item.id}}" value=clicked> <input type="text" value="{{item.text}}" name=n{{item.id}}> <br>
        {% endif %}
        {% endfor %}
        <button type="submit" name="save" value="save"> save</button>
{% endblock %}
```

With the use of curly brackets we can call the variables and also with added percentage sign we can even program some python syntax into our template.
Since Python can't read indentations in the HTML File we have to declare where for loops and if statement end.



Now if we open 127.0.0.1:8000/lists/1, we should see our To Do List with its name and all Items we have created. Depending on if the task is completed or not, it will show as checked if it's complete or unchecked if not.

![Django_Basic_TodoItem](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Django_Basic_TodoItem.png)

To make our application more interactive and to edit our To-Do's in one single template, instead of going to the admin Page and adding them by hand, we can make use of POST forms, which will allow us to send data from our browser to the webserver.

We can make use of forms. For that, create another file: todolist/forms.py.

```python
from django import forms

class todoForm(forms.Form):
    text = forms.CharField(label = 'To-Do',max_length=300)
    complete = forms.BooleanField(required=False)

class todoListForm(forms.Form):
    name = forms.CharField(label = 'To-Do List Name',max_length=300)
```

This will create our Form automatically with all our database fields.


Now we can reference those two forms in our todo/views.py view.

```python
from django.shortcuts import render,redirect

from .models import ToDoList,items
from .forms import todoForm, todoListForm

def ToDoView(request,id):
    list = ToDoList.objects.get(id=id)
    items = list.items_set.all()
    form = todoForm()
    return render(request,'todolist/html/todolist.html',{"list":list,"items":items,"form": form})
```



Basically we are able to add the form in our dictionary as a context and use it as a variable in our template.

Now let's add it into the todolist.html.

```html
{% extends 'todolist/html/base.html' %}

{% block title %}
    {{list}}
{% endblock %}

{% block content %}
     <h1>{{ list }}</h1>
    <form method="POST">
        {% csrf_token %}
        {% for item in items %}
            {% if item.complete == True %}
        <input type="checkbox" name="c{{item.id}}" value=clicked checked> <input type="text" value="{{item.text}}" name=n{{item.id}}>  <br>
        {% else %}
        <input type="checkbox" name="c{{item.id}}" value=clicked> <input type="text" value="{{item.text}}" name=n{{item.id}}> <br>
        {% endif %}
        {% endfor %}
        <button type="submit" name="save" value="save">Save</button>
    </form>
    <p>
        Add To-Do
    </p>
        <form method="POST">
            {{form.as_p}}
            {% csrf_token %}
            <button type="submit" name="addTodo" value="addTodo"> Add To-Do</button>
        </form>
        <form method="POST">
            {% csrf_token %}
            <button type="submit" name="deleteList" value="deleteList"> Delete To-Do List</button>
        </form>
{% endblock %}
```

This will now allow us to check or uncheck tasks. To POST Data we have to add a CSRF token in the POST form element.

The CSRF token, is random string of characters, which also will be POSTed and verify that you used the form. For example if wouldn't have a CSRF token and visit a malicious site, the site could send a form to our site and manipulate it. For more information: "https://en.wikipedia.org/wiki/Cross-site_request_forgery"

Additionally I added an "add to do" and "delete the List".


With a Button with the type "submit" it will POST it's Data to the Server. In this case it will look for all names and values and form a hashtable with all information in the form.
For Example:

```
{
c24: clicked
c25: clicked
save: save
}

```

The first to elements c24,c25 are the checked checkboxes, if those checkboxes are not checked, they won't appear in the Form Data.
The "save" element refers to the clicked button.


The only problem is, our server doesn't know what to do with the POSTed Data he gets.
We will have to edit the views.py and add a few if conditions.

```python
def ToDoView(request,id):
    list = ToDoList.objects.get(id=id)
    items = list.items_set.all()
    form = todoForm()
    if request.method == 'POST':
        if request.POST.get('addTodo'):
            form = todoForm(request.POST)
            if form.is_valid():
                text = form.cleaned_data['text']
                complete = form.cleaned_data['complete']
                list.items_set.create(text=text,complete=complete)
        elif request.POST.get('save'):
            for item in items:
                if request.POST.get('c'+ str(item.id)) == 'clicked':
                    item.complete = True
                    name = request.POST.get('n'+ str(item.id))
                    item.text = name
                else:
                    item.complete = False
                    name = request.POST.get('n'+ str(item.id))
                    item.text = name
                item.save()
        elif request.POST.get('deleteList'):
            list.delete()
            return redirect(IndexView)

    return render(request,'todolist/html/todolist.html',{"list":list,"items":items,"form": form})
```

If we look at the Code, it first looks if the request he gets, is a POST

```python
    if request.method == 'POST':
```
Depending on which button was clicked it executes different code.

Add To-Do button:
Gets the data in the form and creates a new database entry
```python
        if request.POST.get('addTodo'):
            form = todoForm(request.POST)
            if form.is_valid():
                text = form.cleaned_data['text']
                complete = form.cleaned_data['complete']
                list.items_set.create(text=text,complete=complete)
```

Save Button:
Gets the checkbox and textfield data and updates the database entrys corresponding to the item id
```
        elif request.POST.get('save'):
            for item in items:
                if request.POST.get('c'+ str(item.id)) == 'clicked':
                    item.complete = True
                    name = request.POST.get('n'+ str(item.id))
                    item.text = name
                else:
                    item.complete = False
                    name = request.POST.get('n'+ str(item.id))
                    item.text = name
                item.save()
```
Delete To-Do List:
Gets the id of the list and deletes the entry.

```
        elif request.POST.get('deleteList'):
            list.delete()
            return redirect(IndexView)
```

![Django_Basic_TodolistForm](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Django_Basic_TodolistForm.png)


To view all our created To Do Lists, we have to create another template for our overview page.
For that we create a index.html file and place it into our templates folder and define a new function in our views.py.

```html
{% extends 'todolist/html/base.html' %}
{% block title %}
To Do List Overview
{% endblock %}

{% block content %}
    {% for todolist in list %}
    <a href="{{todolist.id}}/">{{todolist}}</a><br>
    {% endfor %}
    <form method="POST">
        {{form.as_p}}
        {% csrf_token %}
        <button type="submit" name="addTodoList" value="addTodoList">Add List</button>
    </form>
{% endblock %}
```

If we take a closer look, I loop through varibale named list, and create an a tag with a link to the id of the list.
Also I added another form, so we can add new To Do Lists.

Now we only have to pass variables with the form and lists from django.

```python
def IndexView(request):
    list = ToDoList.objects.all() # Gets all To Do Lists and places them into a list
    form = todoListForm()
    if request.POST.get('addTodoList'):
        form = todoListForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            list.create(name=name)
        list = ToDoList.objects.all()
        form = todoListForm()

    return render(request,'todolist/html/index.html',{"list":list,'form':form})
```

Until now we have create our own Database Model, created Templates, added a function to add To-Do's to our List , delete the List we currently work on and check or uncheck our To-Do's for completion and have a overview page for all our To-Do lists, where we also can create new lists.



### Adding Bootstrap

Since our basic functions are set, we can add some css and javascript, to make our website look better generally.

We will use Bootstrap to make it easier, since I don't have an indepht knowledge in css and javascript.
First of all we have to add the css Templates into our base.html template.

Simply add this at the top of the head element.

```html
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
```

Now we can use all css classes that are definded.
I used the Documentation at https://getbootstrap.com/docs/5.0/getting-started/introduction/ to get to know a few usable classes.

Let's get started by adding a new navigation bar.

```html
        <div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="/lists">Our To-Do List App</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                          <a class="nav-link active" aria-current="page" href="/lists">Home</a>
                        </li>
                      </ul>
                    </div>
                  </div>
            </nav>
        </div>
```
it should look something like this:
![Bootstrap_Navbar](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Bootstrap_Navbar.png)
Next we add a class to our content block, to center our elements

```html
        <div class="container" id="content", name="content">
            {% block content %}
            {% endblock %}
        </div>
```



```
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">


        <title>
            {% block title %}
            {% endblock %}
        </title>
    </head>
    <body>
<!-- Bootstrap NavBar -->
        <div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="/lists">Our To-Do List App</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                          <a class="nav-link active" aria-current="page" href="/lists">Home</a>
                        </li>
                      </ul>
                    </div>
                  </div>
            </nav>
        </div>

        <div class="container" id="content", name="content">
            {% block content %}
            {% endblock %}
        </div>
    </body>
</html>
```



Now we edit our index.html.
This will add a card looking elements for the links to the To-Do Lists and add a darker background. Addionally we get a new blue button.

```html
{% extends 'todolist/html/base.html' %}
{% block title %}
To Do List Overview
{% endblock %}

{% block content %}
<div class="d-grid gap-3">
    <h2 class="mt-3">All To-Do Lists</h1>
    <div class="p-2 bg-light">
        <div class="card" style="width: 18rem;">
        {% for todolist in list %}
        <a style="color: black;text-decoration: none;" href="{{todolist.id}}/"><li class="list-group-item">{{todolist}}</li></a>
        {% endfor %}
        </div>
    </div>
</div>




<div class="d-grid gap-3">
    <h2 class="mt-3">Add To-Do List</h1>
    <form method="POST">
        <div class="p-2 bg-light">
        {{form.as_p}}
        </div>
        {% csrf_token %}
        <button class="btn btn-primary btn-sm" type="submit" name="addTodoList" value="addTodoList">Add List</button>
    </form>
</div>
{% endblock %}
```

![Bootstrap_Overview](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Bootstrap_Overview.png)

Let's continue with the To-Do list Template todolist.html

```html
{% extends 'todolist/html/base.html' %}

{% block title %}
    {{list}}
{% endblock %}

{% block content %}
<div class="d-grid gap-3">
    
    <h2 class="mt-3">{{ list }}</h2>
    <form method="POST">
        {% csrf_token %}
        {% for item in items %}
            {% if item.complete == True %}
        <div class="p-2 bg-light">
            <input type="checkbox" name="c{{item.id}}" value=clicked checked> <input type="text" value="{{item.text}}" name=n{{item.id}}>
            <button class="btn btn-outline-dark btn-sm" name=deletetodo value={{item.id}}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                </svg>
            </button>
        </div>
        
        {% else %}
        <div class="p-2 bg-light ">
            <input type="checkbox" name="c{{item.id}}" value=clicked> <input type="text" value="{{item.text}}" name=n{{item.id}}>
            <button class="btn btn-outline-dark btn-sm" name=deletetodo value={{item.id}}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                </svg>
            </button>
        </div>

        {% endif %}
        {% endfor %}
        <div class=" p-2 bg-light">
        <button class="btn btn-primary btn-sm " type="submit" name="save" value="save">Save</button>
        </div>
    </form>
</div>

<div class="d-grid gap-3">
        <h2 class="mt-3">Add To-Do</h2>
        
        <form method="POST">
            <div class="p-2 bg-light">
            {{form.as_p}}
            </div>
            {% csrf_token %}
            <div class=" p-2 bg-light">
            <button class="btn btn-primary btn-sm" type="submit" name="addTodo" value="addTodo"> Add To-Do</button>
        </div>
        </form>
        <form method="POST">
            {% csrf_token %}
            <button class="btn btn-primary btn-sm" type="submit" name="deleteList" value="deleteList"> Delete To-Do List</button>
        </form>
</div>
{% endblock %}
```

We simply added the same elements as in the index.html.
Also there is a new trash can icon next to the To-Do Items.

![Bootstrap_ToDoList](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Bootstrap_ToDoList.png)


### Adding New Feature to delete a to do list

Now that our app looks little better, we can add a new functionality, with our newly added Trashcan Icon.
For that we edit the views.py again.

And add some new lines of code to the ToDoView Function.

```python
        elif request.POST.get('deletetodo'):
            delid = request.POST['deletetodo']
            todo =list.items_set.filter(id=delid)
            todo.delete()
```

This will run, if the button with the name "deletetodo" is clicked. The value of this button will be the id of the item in the same row and is defined with "delid".
Next we filter with the ID of the element to get the database object.
If we have the Database Object, we can simply use the delete function on the object.

## Adding  User Registration, Login and User specific sites

In some cases we maybe want that other users can log in to our website and create their own To Do Lists.
For that django has a convenient way to do this, where we only have to manage the forms.

### User Registration

First of all we start a new app. The advantage is that we can reuse this component in other Projects.

for that we type:

```
python manage.py startapp registration
```

As we already kow this will create a new Directory Structure in our Project folder.
As we did with the ToDoList App we also have to register the app in our project's settings.py



next we add a new url in our mysite/urls.py and import the views from the registration app

```
[...]
from registration import views as v
[...]
path('register',v.registration, name='register')
```

This will refer to the views of the register app and execute the function.
As we still don't have created a function within the registration/views.py it will lead nowhere

So lets create it with in-build functions in django.

```
from django.shortcuts import render
from django.contrib.auth import login,authenticate
from django.controb.auth.forms import UserCreationForm

def registration(response):
	form = UserCreationForm()
    return render(response, "register/register.html", {"form":form})
```

Now if we look at it, we see that our function uses a UserCreationForm function, which will create a registration From for new users.
This form is defined as a variable which we will pass to the render function, which is then rendered onto the register/html/register.html file and passed to the user.

As we don't have created our register.html, let's create them and add the form to it.


Again we create a template folder within our app. and create a new file in registration/templates/registration/html/register.html

```
{% extends "todolist/base.html" %}

{% block title %}Account registration{% endblock %}

{% block content %}
	{% csrf_token %}
	<form method="POST" class=form-group">
		{{form}}
		<button type="submit" class="btn btn-success">Register</button>
	</form>
{% endblock %}
```

This will take the form, we defined before in our registration function and display it without any further action.
Also we added some CSS to the Form in general and to the button from Bootstrap.

It should look something like this:

![Django_registration_NoCrispy](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/Django_registration_NoCrispy.png)


Now this still looks clustered, so we have to make some adjustments.
Also our registration page won't work right now, because we don't have defined what django should do with the POSTed data.

So let's start with the registration for now.

We just add an if/else statement, which takes the POSTed data and uses it in the UserCreationForm class, check if the inputed data is valid and finally save it to the database.

```
def registration(response):
    if response.method == "POST":
        form = UserCreationForm(response.POST)
        if form.is_valid():
            form.save()
    else:
        form = UserCreationForm()
    return render(response, "registration/html/register.html", {"form":form})
```





![DjangoAdmin_UserList](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/DjangoAdmin_UserList.png)

If we look into the user itself, we can see some features, that django provides by default.

![DjangoAdmin_User](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/DjangoAdmin_User.png)

First of all, we see that the password is one-way encrypted. That means we are not able to see the passwords, even if we have access to the database itself. In a more technical way, the password is hashed with an algorithm, in this case sha256 and salted with a random string, to get a new hash. So even if two users have the same password, their hash is always different.


### Adding a New App
As for now the registration page looks a bit clustered.

For that we can use an already created app, which can be installed with pip.

```
pip install django-crispy-forms
```

We install the package, register it in the settings.py under INSTALLED_APPS.
Add a new variable as CRISPY_TEMPLATE_PACK and define the CSS pack, in our case we use "bootstap4".
When that's done we can use it everywhere we want, by loading the Package with:

```
{% load crispy_forms_tags %}
```

And by piping a variable with a form inside we can apply the style to it.

For Example:

```
{{form|crispy}}
```

It should look something like this:

![CrispyDjango_Registration](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/CrispyDjango_Registration.png)

### Managing User Authentication and access

First of all we have to let users login and logout.

mysite/urls.py

```
[...]
from django.contrib.auth import views as auth_views

urlpatterns = [
	[...]
    path('login/', auth_views.LoginView.as_view(redirect_authenticated_user=True), name='login'),  # Redirects users to the defined place
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),							   # in settings.py
]
```

create a new template

registration/templates/registration/login

```
{% extends "todolist/html/base.html" %}

{% block title %}
Login
{% endblock %}

{% load crispy_forms_tags %}
{% block content %}
<form method="post" class="form-group">
    {% csrf_token %}
    {{form|crispy}}
    <p><a href="/register">Create Account</a></p>
    <button type="submit" class="btn btn-success">login</button>
    
</form>
{% endblock %}
```



Adjust our base template

Add this to the navigation bar section, which will show a login/logout button on the right side

```
<ul class="navbar-nav mr-auto mb-2 mb-lg-0">
    {% if  user.is_authenticated %}
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/logout">Logout</a>
        </li>
        {% else %}
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/login">Login</a>
        </li>
    {% endif %}
</ul>
```



Now that we have the ability to register new users and let them login, we can look into, how to only let them view pages and to do lists they have created.
At the moment our pages are accessible to anyone.

For an overview, we have to adjust some things to our database models, the todolist view and templates.

todolist/models.py

```
[...]
from django.contrib.auth.models import User

class ToDoList(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
[...]
```

To make changes to the database we have to delete our existing database and migrations in todolist/migrations/{000x_....py}. In case you want to keep your existing database. You can also specify a default value for your old to do list entries in your database, for that you can add a parameter to the new user variable with "default = {your username}".

After that is done we can migrate our changes with:

```
python manage.py makemigrations
python manage.py migrate
```

Now if we look on the administration tab (in case you have deleted your database, you have to create a new superuser) under the to do lists table.

![DjangoAdmin_UserToToDoList](https://gitlab.com/Jomorasch/django-to-do-list-webapp/-/raw/master/img/DjangoAdmin_UserToToDoList.png)

We can see, we can add a user to a to do list.

Now this will still do nothing and everyone can see your to do lists, even if they are not logged in.

What we will do now is, edit our todolist/views.py, require a user is logged in and let them only view to do lists they have created.

```python
def IndexView(request):
    if request.user.is_authenticated: 				    # Check if a user is logged in
        user = request.user			 			   	   # get the user object from the request
        list = ToDoList.objects.filter(user=user) 	     # Only get to do lists with his user object assigned to it
        form = todoListForm()
        if request.POST.get('addTodoList'):
            form = todoListForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data['name']
                list.create(name=name,user=user)
            list = ToDoList.objects.filter(user=user)
            form = todoListForm()
        return render(request,'todolist/html/index.html',{"list":list,'form':form})
    else:							                  # if a user is not logged in, redirect them to the login page
        return redirect('/login')
```



This will essentially only show the user's to do lists, but he is still able to manually go to a to do lists page by typing in the id into the URL like: "/lists/2".

Adjust the todolist/views.py's ToDoView

```python
def ToDoView(request,id):
    if request.user.is_authenticated: 				   # Check if user is logged in
        user = request.user			 				  # get the user object from the request
        try:
            list = ToDoList.objects.get(id=id,user=user)# Try to get the todolist with the id and user object.
        except:										 # If there is no to do list it will except.
            return redirect("/lists")				   # Return them to their overview page, as an alternative, we can redirect them to 
        items = list.items_set.all()				   # a access denied page.
        form = todoForm()
    [...]
    else:
    	return redirect('/login')
```

# Outro and Reflection

This is it of this tutorial creating a web application with Django.
In this Tutorial I went through all basics to create a web application.

We've learned to:
- Create a new django project
- Create a new django app
- Manage HTML Templates
- Manage the sqlite database
- Program a to do list
- Add Basic CSS
- Manage users and authentication

I had a lot of fun working on this project and think that django is one of the best frameworks you can work with as a systemengineer to create dynamic automation sites.
I also learned a few new things by going through the PCAP Course and Django Documentation, which I also can Recommend to everyone who doesn't have an indepth knowledge in python yet.

Last but not least, I hope you liked my Tutorial on Django.

## Time Planning an References

For this Tutorial, I used a video playlist as reference which a tried to recreate.
Beforehand I looked through the basic concepts of Django with the official documentation.

Below is a list of references, which I used and how much Time it took to work through it.

|                             What                             | Time | Link                                                         |
| :----------------------------------------------------------: | ---- | ------------------------------------------------------------ |
|                Refreshing Python Fundementals                | 2h   | PCAP - https://edube.org/login (Only accessible with an account or with an enrolled Cisco Course) |
|                Django Documentation Tutorial                 | 3h   | https://docs.djangoproject.com/en/3.1/intro/tutorial01/      |
|                 in depth Research of Django                  | 4h   | https://docs.djangoproject.com/en/3.1/                       |
| Watching a Tutorial on how to build a Full Stack Website with Python | 3h   | https://www.youtube.com/playlist?list=PLzMcBGfZo4-kQkZp-j9PNyKq7Yw5VYjq9 |
|               CSS with Bootstrap fundamentals                | 2h   | https://getbootstrap.com/docs/5.0/getting-started/introduction/ |

Total Time for Research: about 14 Hours

Below is also another list how much time to it took to create this Tutorial and to code the website.

| What                              | Time |
| --------------------------------- | ---- |
| Coding - Basic To Do List         | 3h   |
| Coding - Adding advanced Features | 3h   |
| Writing/Structuring the Tutorial  | 10h   |

Total Time: about 27 Hours



