from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm



def registration(response):
    if response.method == "POST":
        form = UserCreationForm(response.POST)
        if form.is_valid():
            form.save()
            return redirect('/lists')
    else:
        form = UserCreationForm()
    return render(response, "registration/html/register.html", {"form":form})
