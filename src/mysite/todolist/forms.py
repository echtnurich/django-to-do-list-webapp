from django import forms

class todoForm(forms.Form):
    text = forms.CharField(label = 'To-Do',max_length=300)
    complete = forms.BooleanField(required=False)

class todoListForm(forms.Form):
    name = forms.CharField(label = 'To-Do List Name',max_length=300)