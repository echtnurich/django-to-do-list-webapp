from django.shortcuts import render,redirect
from .models import ToDoList,items
from .forms import todoForm, todoListForm


def IndexView(request):
    if request.user.is_authenticated:
        user = request.user
        list = ToDoList.objects.filter(user=user)
        form = todoListForm()
        if request.POST.get('addTodoList'):
            form = todoListForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data['name']
                list.create(name=name,user=user)
            list = ToDoList.objects.filter(user=user)
            form = todoListForm()
        return render(request,'todolist/html/index.html',{"list":list,'form':form})
    else:
        return redirect('/login')

def ToDoView(request,id):
    if request.user.is_authenticated:
        user = request.user
        try:
            list = ToDoList.objects.get(id=id,user=user)
        except:
            return redirect("/lists")
        items = list.items_set.all()
        form = todoForm()
        if request.method == 'POST':
            if request.POST.get('addTodo'):
                form = todoForm(request.POST)
                if form.is_valid():
                    text = form.cleaned_data['text']
                    complete = form.cleaned_data['complete']
                    list.items_set.create(text=text,complete=complete)
            elif request.POST.get('save'):
                for item in items:
                    if request.POST.get('c'+ str(item.id)) == 'clicked':
                        item.complete = True
                        name = request.POST.get('n'+ str(item.id))
                        item.text = name
                    else:
                        item.complete = False
                        name = request.POST.get('n'+ str(item.id))
                        item.text = name
                    item.save()
            elif request.POST.get('deleteList'):
                list.delete()
                return redirect(IndexView)
            elif request.POST.get('deletetodo'):
                delid = request.POST['deletetodo']
                todo =list.items_set.filter(id=delid)
                todo.delete()
        return render(request,'todolist/html/todolist.html',{"list":list,"items":items,"form": form})
    else:
        return redirect('/login')

def welcomeView(request):
    return render(request,'todolist/html/welcome.html',{})